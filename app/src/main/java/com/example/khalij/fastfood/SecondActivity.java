package com.example.khalij.fastfood;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    String restId;
    String botNav = "";
    Menu m;
    Typeface type;
    TextView titleTextView;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.nav_rest_info:
                    if (!botNav.equals("1")) {
//                        setTitle(R.string.rest_info);
                        titleTextView.setText(R.string.rest_info);
                        FragmentManager fragmentManager2 = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                        RestaurantInfoActivityFragment fragment2 = new RestaurantInfoActivityFragment().newInstance(restId);
                        fragmentTransaction2.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit);
                        fragmentTransaction2.replace(R.id.content_second, fragment2);
                        fragmentTransaction2.commit();
                        botNav = "1";
                    }
                    return true;
                case R.id.nav_rest_locate:
                    if (!botNav.equals("2")) {
                        FragmentManager fragmentManager3 = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction3 = fragmentManager3.beginTransaction();
                        MapActivity2Fragment fragment3 = new MapActivity2Fragment().newInstance(restId,"location");
                        fragmentTransaction3.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit);
                        fragmentTransaction3.replace(R.id.content_second, fragment3);
                        fragmentTransaction3.commit();
                        botNav = "2";
                    }
                    return true;
                case R.id.nav_rest_ratings:
                    if (!botNav.equals("3")) {
//                        setTitle(R.string.ratings);
                        titleTextView.setText(R.string.ratings);
                        FragmentManager fragmentManager4 = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction4 = fragmentManager4.beginTransaction();
                        RatingsActivityFragment fragment4 = new RatingsActivityFragment();
                        fragmentTransaction4.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit);
                        fragmentTransaction4.replace(R.id.content_second, fragment4);
                        fragmentTransaction4.commit();
                        botNav = "3";
                    }
                    return true;
                case R.id.nav_rest_menu:
                    if (!botNav.equals("4")) {
                        titleTextView.setText(R.string.types);
                        FragmentManager fragmentManager5 = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction5 = fragmentManager5.beginTransaction();
                        MenuActivityFragment fragment5 = new MenuActivityFragment().newInstance(restId);
                        fragmentTransaction5.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit);
                        fragmentTransaction5.replace(R.id.content_second, fragment5);
                        fragmentTransaction5.commit();
                        botNav = "4";
                    }
                    return true;
            }
            return true;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        setTitle(R.string.rest_info);
        initViews();
        restId=getIntent().getStringExtra("rest_id");


        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        final ActionBar actionBar = getSupportActionBar();

        // actionBar
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // titleTextView
        titleTextView = new TextView(actionBar.getThemedContext());
        titleTextView.setText("الرئيسية");
//        titleTextView.setGravity(Gravity.CENTER);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            titleTextView.setForegroundGravity(Gravity.CENTER);
//        }
        titleTextView.setTypeface(type);

        // Add titleTextView into ActionBar
        actionBar.setCustomView(titleTextView);



        initUI();
        initEventDriven();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation_second);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        m = navigation.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

    }

    private void initViews() {

    }

    private void initUI() {


    }

    private void initEventDriven() {

    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

}
