package com.example.khalij.fastfood;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by elban on 2016-07-31.
 */
public class MapActivity2Fragment extends android.support.v4.app.Fragment
        implements OnMapReadyCallback {
    StringRequest stringRequest;
    View view;
    GoogleMap mMap;
    ArrayList<String> x;
    double lat, lng;
    LocationManager locationManager;
    Location myLocation;
    String id, type;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        id = getArguments().getString("restId");
        type = getArguments().getString("type");

        if (container == null) {
            return null;
        }
        view = inflater.inflate(R.layout.fragment_map, container, false);
        x = new ArrayList<>();
        return view;
    }

    void setUpMapIfNeeded() {
        if (mMap == null) {
            SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.location_map);
            mapFrag.getMapAsync(this);
            //To get Current Location
//            mMap = ((SupportMapFragment) NearestActivity.fragmentManager
//                    .findFragmentById(R.restId.location_map)).getMap();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        if (mMap != null)
            setUpMapIfNeeded();
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.location_map);
            mapFrag.getMapAsync(this);
//            mMap = ((SupportMapFragment) getChildFragmentManager()
//                    .findFragmentById(R.restId.location_map)).getMapAsync(this);
            // Check if we were successful in obtaining the map.
//            if (mMap != null)
//                // For showing a move to my loction button
//                mMap.setMyLocationEnabled(true);
            // For dropping a marker at a point on the Map
            ////////////////////////////////////////////////////////////////
//            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(30.01, 31.14), 5));
//            countriesVolley("https://restcountries.eu/rest/v1/all");
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMapIfNeeded();

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        currentLoc();


//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 5));

    }


    public void locationVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(getActivity()); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("رجاء الإنتظار...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    JSONObject jsonObject = new JSONObject(response);

//                    jsonObject.getString("name");
                    dialog.dismiss();
                    LatLng latLng = new LatLng(Double.parseDouble(jsonObject.getJSONObject("branch").get("lat").toString()), Double.parseDouble(jsonObject.getJSONObject("branch").get("lng").toString()));
                    mMap.addMarker(new MarkerOptions().position(latLng).title(jsonObject.getJSONObject("branch").get("ar_name").toString())
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.mark)));
                    Log.d("latlng Search", latLng.toString());
                    Geocoder geocoder = new Geocoder(getContext());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                        // String str = addresses.get(0).getLocality() + ",";
                        // str += addresses.get(0).getCountryName();
//                        mMap.addMarker(new MarkerOptions().position(latLng).title(str));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getActivity(), "حدث خطأ ما..!", Toast.LENGTH_LONG).show();
            }
        });
        Volley.newRequestQueue(getActivity()).add(stringRequest);

    }

    public void nearestVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(getActivity()); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("رجاء الإنتظار...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    JSONObject jsonObject = new JSONObject(response);
                    Double.parseDouble(jsonObject.getString("lng"));
//                    jsonObject.getString("name");
                    dialog.dismiss();
                    LatLng latLng = new LatLng(Double.parseDouble(jsonObject.getString("lat")), Double.parseDouble(jsonObject.getString("lng")));
                    mMap.addMarker(new MarkerOptions().position(latLng).title(jsonObject.getString("ar_name"))
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.mark)));
                    Log.d("latlng Search", lat + " ... " + lng);


                    Geocoder geocoder = new Geocoder(getContext());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                        // String str = addresses.get(0).getLocality() + ",";
                        // str += addresses.get(0).getCountryName();
//                        mMap.addMarker(new MarkerOptions().position(latLng).title(str));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getActivity(), "حدث خطأ ما..!", Toast.LENGTH_LONG).show();
            }
        });
        Volley.newRequestQueue(getActivity()).add(stringRequest);

    }

    public static MapActivity2Fragment newInstance(String id, String type) {
        MapActivity2Fragment mapActivity2Fragment = new MapActivity2Fragment();
        Bundle bundle = new Bundle();
        bundle.putString("restId", id);
        bundle.putString("type", type);
        mapActivity2Fragment.setArguments(bundle);
        return mapActivity2Fragment;
    }

    private void currentLoc() {
        Log.d("Lat&Lng", "On Current Location");
        final LocationManager[] locationManager = {(LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE)};
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            return;
//        }
        if (ActivityCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
        }
        if (locationManager[0].isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Log.d("Lat&Lng", "NETWORK_PROVIDER");
            locationManager[0].requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.d("Lat&Lng", "onLocationChanged");
                    lng = location.getLongitude();
                    lat = location.getLatitude();
//                Log.d("link","http://www.masaya.khalijalbarmaja.com.sa/api/search/" +
//                        lat +
//                        "/" +
//                        lng);
//                countriesVolley("http://www.masaya.khalijalbarmaja.com.sa/api/search/" +
//                        lat +
//                        "/" +
//                        lng);
                    LatLng latLng = new LatLng(lat, lng);


                    if (type == "nearest") {
                        Log.e("UUURRRLLL", Connection.BaseUrl+"branch/nearest/lat/" + lat +
                                "/lng/" + lng);
                        Log.e("TYPE", "NEAR");
                        nearestVolley(Connection.BaseUrl+"branch/nearest/lat/" + lat +
                                "/lng/" + lng);
                    } else {
                        Log.e("UUURRRLLL", Connection.BaseUrl+"branch/" + id);
                        Log.e("TYPE", "LOCATION");
                        locationVolley(Connection.BaseUrl+"branch/" + id);
                    }
                    Geocoder geocoder = new Geocoder(getContext());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    locationManager[0].removeUpdates(this);
                    locationManager[0] = null;
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        } else if (locationManager[0].isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.d("Lat&Lng", "GPS_PROVIDER");
            locationManager[0].requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.d("Lat&Lng", "onLocationChanged");
                    lng = location.getLongitude();
                    lat = location.getLatitude();
//                if(intent.getAction().equals("nearest"))
//                {
//                    Log.d("link","http://www.masaya.khalijalbarmaja.com.sa/api/search/" +
//                            lat +
//                            "/" +
//                            lng);
//                    countriesVolley("http://www.masaya.khalijalbarmaja.com.sa/api/search/" +
//                            lat +
//                            "/" +
//                            lng);
//                }
                    LatLng latLng = new LatLng(lat, lng);
                    Log.d("Lat&Lng", "current Loc Lat = " + lat + " lng = " + lng);
                    Geocoder geocoder = new Geocoder(getContext());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                        // String str = addresses.get(0).getLocality() + ",";
                        //str += addresses.get(0).getCountryName();
//                        mMap.addMarker(new MarkerOptions().position(latLng));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    locationManager[0].removeUpdates(this);
                    locationManager[0] = null;
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }
        Log.e("LAT", lat + "");
        Log.e("LNG", lng + "");
    }
}
