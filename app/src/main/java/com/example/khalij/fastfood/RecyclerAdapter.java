package com.example.khalij.fastfood;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by elban on 2016-07-19.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyHolder> {
    Typeface type;
    Context context;
    private List<ModelRestaurant> list;


    public RecyclerAdapter(List<ModelRestaurant> myList) {
        this.list = myList;


    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row_item, parent, false);
        context = parent.getContext();
        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyHolder holder, int position) {
        final ModelRestaurant model = list.get(position);
        holder.textViewName.setText(model.getName());
        if(model.getInfo().length()>20) {
            holder.textViewInfo.setText(model.getInfo().subSequence(0, 20) + "...");
        }else{
            holder.textViewInfo.setText(model.getInfo());
        }
        Picasso.with(holder.itemView.getContext()).load(Connection.ImageUrl+model.getImageURL()).into(holder.imageView);
        type = Typeface.createFromAsset(context.getAssets(), "fonts/din_next_medium.ttf");
        holder.textViewName.setTypeface(type);
        type = Typeface.createFromAsset(context.getAssets(), "fonts/din_next_light.ttf");
        holder.textViewInfo.setTypeface(type);
//        if((position!=0&&position%2==0)||(position==0)){
//            holder.cardView.setLayoutParams(new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150));
//        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyHolder extends RecyclerView.ViewHolder {
        public TextView textViewName, textViewInfo;
        public ImageView imageView;
        public CardView cardView;

        public MyHolder(View view) {
            super(view);
            textViewName = (TextView) view.findViewById(R.id.titleListItem);
            textViewInfo = (TextView) view.findViewById(R.id.infoListItem);
            imageView = (ImageView) view.findViewById(R.id.imageViewListItem);
            cardView=(CardView)view.findViewById(R.id.cardViewMain);
        }
    }
}
