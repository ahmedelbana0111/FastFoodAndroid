package com.example.khalij.fastfood;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

/**
 * A placeholder fragment containing a simple view.
 */
public class SignUpFamilyActivityFragment extends Fragment {
    View view;
    Typeface type;
    EditText editTextName,editTextPhone,editTextMail,editTextPassword,editTextConfirmPassword,editTextCivil;
    Spinner spinnerCountry,spinnerTown,spinnerDistrict,spinnerPeriod,spinnerCategory;
    Button buttonSignUp;
    public SignUpFamilyActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_sign_up_family, container, false);
        initViews();
        initUI();
        initEventDriven();
        return view;
    }

    private void initViews() {
        editTextName=(EditText)view.findViewById(R.id.editTextName_signUpFamily);
        editTextPhone=(EditText)view.findViewById(R.id.editTextPhone_signUpFamily);
        editTextMail=(EditText)view.findViewById(R.id.editTextMail_signUpFamily);
        editTextPassword=(EditText)view.findViewById(R.id.editTextPassWord_signUpFamily);
        editTextConfirmPassword=(EditText)view.findViewById(R.id.editTextConfirmPassWord_signUpFamily);
        editTextCivil=(EditText)view.findViewById(R.id.editTextCivil_signUpFamily);
        spinnerCountry=(Spinner)view.findViewById(R.id.spinnerCountry_signUpFamily);
        spinnerTown=(Spinner)view.findViewById(R.id.spinnerTown_signUpFamily);
        spinnerDistrict=(Spinner)view.findViewById(R.id.spinnerDistrict_signUpFamily);
        spinnerPeriod=(Spinner)view.findViewById(R.id.spinnerPeriod_signUpFamily);
        spinnerCategory=(Spinner)view.findViewById(R.id.spinnerCategory_signUpFamily);
        buttonSignUp=(Button)view.findViewById(R.id.buttonSignUp_signUpFamily);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_regular.ttf");
        buttonSignUp.setTypeface(type);
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_light.ttf");
        editTextPhone.setTypeface(type);
        editTextName.setTypeface(type);
        editTextConfirmPassword.setTypeface(type);
        editTextPassword.setTypeface(type);
        editTextMail.setTypeface(type);
        editTextCivil.setTypeface(type);
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_medium.ttf");
    }

    private void initEventDriven() {

    }
}
