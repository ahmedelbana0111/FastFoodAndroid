package com.example.khalij.fastfood;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ConfirmPhoneActivity extends AppCompatActivity {
    TextView textViewTitle,textViewWill;
    EditText editTextSms;
    Button buttonContinue,buttonChangePhone;
    Typeface type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_phone);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        textViewTitle=(TextView)findViewById(R.id.textViewTitle_Confirm);
        textViewWill=(TextView)findViewById(R.id.textViewWill_Confirm);
        editTextSms=(EditText)findViewById(R.id.editTextSms_Confirm);
        buttonContinue=(Button)findViewById(R.id.buttonContinue_Confirm);
        buttonChangePhone=(Button)findViewById(R.id.buttonChangePhone_Confirm);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textViewTitle.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        textViewWill.setTypeface(type);
        editTextSms.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_medium.ttf");
        buttonChangePhone.setTypeface(type);
        buttonContinue.setTypeface(type);
    }

    private void initEventDriven() {

    }

}
