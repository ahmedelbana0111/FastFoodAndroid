package com.example.khalij.fastfood;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class CartActivity extends AppCompatActivity {
    TextView textViewCount,textViewCount2,textViewSum,textViewSum2,textViewTitle;
    Button buttonConfirm;
    RecyclerView recyclerView;
    Typeface type;
    RecyclerAdapterCart recyclerAdapter;
    RecyclerView.LayoutManager lm;
    ArrayList<ModelCart> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        arrayList = (ArrayList<ModelCart>) getIntent().getSerializableExtra("arrayListCart");
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        textViewCount=(TextView)findViewById(R.id.textViewCountCart);
        textViewCount2=(TextView)findViewById(R.id.textViewCount2Cart);
        textViewSum=(TextView)findViewById(R.id.textViewSumCart);
        textViewSum2=(TextView)findViewById(R.id.textViewSum2Cart);
        buttonConfirm=(Button) findViewById(R.id.buttonConfirmCart);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerViewCart);
        textViewTitle=(TextView)findViewById(R.id.textViewTitleCart);
    }

    private void initUI() {
        arrayList=new ArrayList<>();
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textViewTitle.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_medium.ttf");
        textViewCount.setTypeface(type);
        textViewSum.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
        textViewCount2.setTypeface(type);
        textViewSum2.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        buttonConfirm.setTypeface(type);
        lm = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(lm);
        recyclerAdapter = new RecyclerAdapterCart(arrayList);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.notifyDataSetChanged();



    }

    private void initEventDriven() {
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
