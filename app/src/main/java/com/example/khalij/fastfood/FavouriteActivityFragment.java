package com.example.khalij.fastfood;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A placeholder fragment containing a simple view.
 */
public class FavouriteActivityFragment extends Fragment {
    View view;
    RecyclerView recyclerView;
    public FavouriteActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_favourite, container, false);
        initViews();
        initUI();
        initEventDriven();
        return view;
    }

    private void initViews() {
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerViewFav);
    }

    private void initUI() {

    }

    private void initEventDriven() {

    }
}
