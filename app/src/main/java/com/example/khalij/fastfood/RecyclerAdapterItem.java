package com.example.khalij.fastfood;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by elban on 2016-07-19.
 */
public class RecyclerAdapterItem extends RecyclerView.Adapter<RecyclerAdapterItem.MyHolder> {
    Typeface type;
    Context context;
    private List<ModelFood> list;


    public RecyclerAdapterItem(List<ModelFood> myList) {
        this.list = myList;
    }


    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row_item_food, parent, false);
        context = parent.getContext();
        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyHolder holder, int position) {
        final ModelFood model = list.get(position);
        holder.textViewName.setText(model.getName());
        if(model.getInfo().length()>20) {
            holder.textViewInfo.setText(model.getInfo().subSequence(0, 20)+"...");
        }else{
            holder.textViewInfo.setText(model.getInfo());
        }
        if(!model.getPrice().toString().equals("")) {
            holder.textViewPrice.setText(context.getResources().getString(R.string.price) + " : " + model.getPrice());
        }
        Picasso.with(holder.itemView.getContext()).load(Connection.ImageUrl+model.getImageURL()).into(holder.imageView);
        type = Typeface.createFromAsset(context.getAssets(), "fonts/din_next_medium.ttf");
        holder.textViewName.setTypeface(type);
        type = Typeface.createFromAsset(context.getAssets(), "fonts/din_next_light.ttf");
        holder.textViewInfo.setTypeface(type);
        holder.buttonAdd.setTypeface(type);
        type = Typeface.createFromAsset(context.getAssets(), "fonts/din_next_regular.ttf");
        holder.textViewPrice.setTypeface(type);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyHolder extends RecyclerView.ViewHolder {
        public TextView textViewName, textViewInfo,textViewPrice;
        public ImageView imageView;
        public Button buttonAdd;

        public MyHolder(View view) {
            super(view);
            textViewName = (TextView) view.findViewById(R.id.nameItem);
            textViewInfo = (TextView) view.findViewById(R.id.infoItem);
            textViewPrice=(TextView)view.findViewById(R.id.priceItem);
            imageView = (ImageView) view.findViewById(R.id.imageItem);
            buttonAdd=(Button)view.findViewById(R.id.addCartItem);
        }
    }
}
