package com.example.khalij.fastfood;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class SecondHomeActivity extends AppCompatActivity {
    String url, type,title;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    ArrayList<ModelRestaurant> arrayListModel;
    RecyclerView.LayoutManager lm;
    Boolean loading = false;
    String nextUrl = "null";
    TextView textViewTitle;
    Typeface typeface;
    FloatingActionButton floatFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //This URL for families || high rests || all rests
        url = getIntent().getStringExtra("url");
        type = getIntent().getStringExtra("type");
        title=getIntent().getStringExtra("title");
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        floatFilter=(FloatingActionButton)findViewById(R.id.fab_filter2);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewSecondHome);
        textViewTitle=(TextView)findViewById(R.id.textViewTitleSecondHome);
    }

    private void initUI() {
        typeface = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textViewTitle.setTypeface(typeface);
        textViewTitle.setText(title);
        lm = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(lm);
        arrayListModel = new ArrayList<>();
        getVolley(url);
    }

    private void initEventDriven() {
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) throws FileNotFoundException {
                        Intent intent=new Intent(SecondHomeActivity.this,SecondActivity.class);
                        intent.putExtra("rest_id",arrayListModel.get(position).getId());
                        startActivity(intent);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                    }
                })
        );
        floatFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SecondHomeActivity.this,FilterActivity.class);
                startActivity(intent);
            }
        });
    }

    public void getVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    if (response != null) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JSONObject results = new JSONObject(response);
                    JSONArray jsonArray = results.getJSONArray("data");
                    if (type.equals("other")) {
                        for (int x = 0; x < jsonArray.length(); x++) {
                            arrayListModel.add(new ModelRestaurant(jsonArray.getJSONObject(x).get("id").toString()
                                    , jsonArray.getJSONObject(x).get("ar_name").toString()
                                    , jsonArray.getJSONObject(x).get("ar_details").toString()
                                    , jsonArray.getJSONObject(x).get("logo").toString()));
                        }
                    } else {
                        for (int x = 0; x < jsonArray.length(); x++) {
                            arrayListModel.add(new ModelRestaurant(jsonArray.getJSONObject(x).get("id").toString()
                                    , jsonArray.getJSONObject(x).get("ar_name").toString()
                                    , jsonArray.getJSONObject(x).get("ar_describe").toString()
                                    , jsonArray.getJSONObject(x).get("photo").toString()));
                        }
                    }
                    nextUrl = results.get("next_page_url").toString();
                    loading = !nextUrl.equals("null");
                    recyclerAdapter = new RecyclerAdapter(arrayListModel);
                    recyclerView.setAdapter(recyclerAdapter);
                    recyclerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error Message", error.getMessage());
                dialog.dismiss();
                initUI();
            }
        });
        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }

}