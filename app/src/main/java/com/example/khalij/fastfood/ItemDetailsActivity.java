package com.example.khalij.fastfood;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ItemDetailsActivity extends AppCompatActivity {
    ImageView imageView;
    TextView textViewFoodName, textViewRestName, textViewDescription, textViewCount, textViewSelectSize1, textViewSelectSize2,
            textViewSelectAdditionals, textViewTitle;
    Spinner spinnerSize;
    LinearLayout linearLayoutAdditionals;
    Button buttonAdd, buttonPlus, buttonMinus;
    Typeface type;
    ArrayList<String> arrayListSize, arrayListSizePrice, arrayListSizes,arrayListAdditionalNames;
    String id;
    FloatingActionButton fabCart;
    ArrayList<ModelCart> arrayListFoods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        id = getIntent().getStringExtra("restId");
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        imageView = (ImageView) findViewById(R.id.imageViewItemDetails);
        textViewFoodName = (TextView) findViewById(R.id.textViewFoodNameItemDetails);
        textViewRestName = (TextView) findViewById(R.id.textViewRestNameItemDetails);
        textViewDescription = (TextView) findViewById(R.id.textViewDescItemDetails);
        textViewCount = (TextView) findViewById(R.id.textViewCountItemDetails);
        textViewSelectSize1 = (TextView) findViewById(R.id.textViewSize1ItemDetails);
        textViewSelectSize2 = (TextView) findViewById(R.id.textViewSize2ItemDetails);
        textViewSelectAdditionals = (TextView) findViewById(R.id.textViewAdditionalsItemDetails);
        spinnerSize = (Spinner) findViewById(R.id.spinnerSizeItemDetails);
        linearLayoutAdditionals = (LinearLayout) findViewById(R.id.layoutAdditionalsItemDetails);
        buttonAdd = (Button) findViewById(R.id.buttonAddItemDetails);
        buttonPlus = (Button) findViewById(R.id.buttonPlusItemDetails);
        buttonMinus = (Button) findViewById(R.id.buttonMinusItemDetails);
        textViewTitle = (TextView) findViewById(R.id.textViewTitleItemDetails);
        fabCart = (FloatingActionButton) findViewById(R.id.fab_item_details);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textViewTitle.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_medium.ttf");
        textViewFoodName.setTypeface(type);
        textViewRestName.setTypeface(type);
        textViewDescription.setTypeface(type);
        textViewCount.setTypeface(type);
        textViewSelectSize1.setTypeface(type);
        textViewSelectSize2.setTypeface(type);
        textViewSelectAdditionals.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        buttonAdd.setTypeface(type);
        buttonMinus.setTypeface(type);
        buttonPlus.setTypeface(type);
        textViewCount.setText("0");
        arrayListSize = new ArrayList<>();
        arrayListSizePrice = new ArrayList<>();
        arrayListSizes = new ArrayList<>();
        arrayListFoods = new ArrayList<>();
        arrayListAdditionalNames=new ArrayList<>();
        getVolley(Connection.BaseUrl+"branch/menu/food/" +
                id +
                "/details");
    }

    private void initEventDriven() {
        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewCount.setText((Integer.parseInt(textViewCount.getText().toString()) + 1) + "");
            }
        });
        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textViewCount.getText().toString().equals("0")) {
                    Toast.makeText(getApplicationContext(), R.string.num_zero, Toast.LENGTH_SHORT).show();
                } else {
                    textViewCount.setText((Integer.parseInt(textViewCount.getText().toString()) - 1) + "");
                }
            }
        });
    }


    public void getVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    if (response != null) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JSONObject results = new JSONObject(response);
                    final JSONObject jsonObject = results.getJSONObject("food");
                    textViewFoodName.setText(jsonObject.get("ar_name").toString());
                    textViewRestName.setText("");
                    textViewDescription.setText(jsonObject.get("ar_additional").toString());
                    Picasso.with(getApplicationContext()).load(Connection.ImageUrl
                            + jsonObject.get("photo").toString()).into(imageView);
                    JSONArray jsonArray = results.getJSONArray("size_price");
                    for (int x = 0; x < jsonArray.length(); x++) {
                        arrayListSize.add(jsonArray.getJSONArray(x).get(0).toString());
                        arrayListSizePrice.add(jsonArray.getJSONArray(x).get(1).toString());
                        arrayListSizes.add(jsonArray.getJSONArray(x).get(0).toString() + "   "
                                + jsonArray.getJSONArray(x).get(1).toString());
                    }
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_view, arrayListSizes) {
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View v = super.getView(position, convertView, parent);
                            Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
                            ((TextView) v).setTypeface(externalFont);
                            ((TextView) v).setTextColor(Color.LTGRAY);
                            ((TextView) v).setTextColor(Color.BLACK);
                            return v;
                        }


                        @Override
                        public boolean isEnabled(int position) {

                            return true;

                        }

                        @Override
                        public View getDropDownView(int position, View convertView,
                                                    ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;

                            tv.setTextColor(Color.BLACK);

                            Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
                            ((TextView) view).setTypeface(externalFont);
                            return view;
                        }
                    };
                    arrayAdapter.setDropDownViewResource(R.layout.spinner_view);
                    spinnerSize.setAdapter(arrayAdapter);

                    final JSONArray jsonArray2 = results.getJSONArray("additionals");

                    for (int x = 0; x < jsonArray.length(); x++) {

                        final CheckBox cb = new CheckBox(getApplicationContext());
                        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (isChecked == true) {
                                    try {
                                        HomeActivity.arrayListAdditionals.add(jsonArray2.getJSONObject(arrayListAdditionalNames.indexOf(cb.getText().toString())).get("id").toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    HomeActivity.arrayListAdditionals.remove(arrayListAdditionalNames.indexOf(cb.getText().toString()));
                                    arrayListAdditionalNames.remove(arrayListAdditionalNames.indexOf(cb.getText().toString()));
                                }
                            }
                        });
                        arrayListAdditionalNames.add(jsonArray2.getJSONObject(x).get("ar_name").toString());
                        cb.setText(jsonArray2.getJSONObject(x).get("ar_name").toString());
                        cb.setTextColor(Color.parseColor("#FF0000"));
                        cb.setTypeface(type);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            cb.setButtonTintList(ColorStateList.valueOf(Color.parseColor("#481245")));
                            linearLayoutAdditionals.addView(cb);
                        }
                    }
                    buttonAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                arrayListFoods.add(new ModelCart(jsonObject.get("id").toString(), jsonObject.get("ar_name").toString()
                                        , jsonObject.get("ar_additional").toString(), jsonObject.get("photo").toString()
                                        , textViewCount.getText().toString(),"foods"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    fabCart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(ItemDetailsActivity.this,CartActivity.class);
                            intent.putExtra("arrayListCart",arrayListFoods);
                            intent.putExtra("additionals",HomeActivity.arrayListAdditionals);
                            startActivity(intent);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error Message", error.getMessage());
                dialog.dismiss();
                initUI();
            }
        });
        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }

}
