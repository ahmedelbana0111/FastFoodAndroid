package com.example.khalij.fastfood;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MyProfileActivity extends AppCompatActivity {
    TextView textViewTitle,textViewName,textViewMail,textViewPhone,textViewCurPass,textViewNewPass,textViewConfirmPass,textViewMyPro;
    EditText editTextName,editTextMail,editTextPhone,editTextCurPass,editTextNewPass,editTextConfirmPass;
    Button buttonChange;
    Typeface type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        textViewTitle=(TextView)findViewById(R.id.textViewTitle_myProfile);
        textViewName=(TextView)findViewById(R.id.textViewName_myProfile);
        textViewMail=(TextView)findViewById(R.id.textViewMail_myProfile);
        textViewPhone=(TextView)findViewById(R.id.textViewPhone_myProfile);
        textViewCurPass=(TextView)findViewById(R.id.textViewCurrentPass_myProfile);
        textViewNewPass=(TextView)findViewById(R.id.textViewNewPass_myProfile);
        textViewConfirmPass=(TextView)findViewById(R.id.textViewConfirmPass_myProfile);
        textViewMyPro=(TextView)findViewById(R.id.textViewMyPro_myProfile);
        editTextName=(EditText)findViewById(R.id.editTextName_myProfile);
        editTextMail=(EditText)findViewById(R.id.editTextMail_myProfile);
        editTextPhone=(EditText)findViewById(R.id.editTextPhone_myProfile);
        editTextCurPass=(EditText)findViewById(R.id.editTextCurrentPass_myProfile);
        editTextNewPass=(EditText)findViewById(R.id.editTextNewPass_myProfile);
        editTextConfirmPass=(EditText)findViewById(R.id.editTextConfirmPass_myProfile);
        buttonChange=(Button)findViewById(R.id.buttonChange_myProfile);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textViewTitle.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_medium.ttf");
        textViewMyPro.setTypeface(type);
        buttonChange.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        textViewName.setTypeface(type);
        textViewMail.setTypeface(type);
        textViewPhone.setTypeface(type);
        textViewCurPass.setTypeface(type);
        textViewNewPass.setTypeface(type);
        textViewConfirmPass.setTypeface(type);
        editTextName.setTypeface(type);
        editTextMail.setTypeface(type);
        editTextPhone.setTypeface(type);
        editTextCurPass.setTypeface(type);
        editTextNewPass.setTypeface(type);
        editTextConfirmPass.setTypeface(type);
    }

    private void initEventDriven() {
        buttonChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

}
