package com.example.khalij.fastfood;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OfferActivity extends AppCompatActivity {
    TextView textViewTitle,textViewName,textViewDetails,textViewPrice,textViewNum;
    Button buttonAdd,buttonPlus,buttonMinus,buttonToRest;
    ImageView imageViewPic;
    String name,price, restId,id,image,description;
    Typeface type;
    FloatingActionButton fabCart;
    ArrayList<ModelCart> arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        restId =getIntent().getStringExtra("restId");
        name=getIntent().getStringExtra("name");
        price=getIntent().getStringExtra("price");
        description=getIntent().getStringExtra("description");
        image=getIntent().getStringExtra("image");
        id=getIntent().getStringExtra("id");
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        textViewTitle=(TextView)findViewById(R.id.textViewTitleOffer);
        textViewName=(TextView)findViewById(R.id.textViewTitle_offer);
        textViewPrice=(TextView)findViewById(R.id.textViewPrice_offer);
        textViewDetails=(TextView)findViewById(R.id.textViewDetails_offer);
        textViewNum=(TextView)findViewById(R.id.textViewNum_offer);
        buttonAdd=(Button)findViewById(R.id.buttonAdd_offer);
        buttonPlus=(Button)findViewById(R.id.buttonPlus_offer);
        buttonMinus=(Button)findViewById(R.id.buttonMinus_offer);
        buttonToRest=(Button)findViewById(R.id.buttonGoToRest_offer);
        imageViewPic=(ImageView)findViewById(R.id.imageViewPic_offer);
        fabCart=(FloatingActionButton)findViewById(R.id.fab_offer);
    }

    private void initUI() {
        arrayList=new ArrayList<>();
        Picasso.with(getApplicationContext())
                .load(Connection.ImageUrl+image).into(imageViewPic);
        textViewName.setText(name);
        textViewPrice.setText(getString(R.string.price)+" : "+price);
        textViewDetails.setText(description);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textViewTitle.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        textViewDetails.setTypeface(type);
        textViewName.setTypeface(type);
        textViewPrice.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
        textViewNum.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_medium.ttf");
        buttonToRest.setTypeface(type);
        buttonMinus.setTypeface(type);
        buttonPlus.setTypeface(type);
        buttonAdd.setTypeface(type);
        textViewNum.setText("0");
    }

    private void initEventDriven() {
        buttonToRest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(OfferActivity.this,SecondActivity.class);
                intent.putExtra("rest_id", restId);
                startActivity(intent);
            }
        });
        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewNum.setText((Integer.parseInt(textViewNum.getText().toString())+1)+"");
            }
        });
        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textViewNum.getText().toString().equals("0")){
                    Toast.makeText(getApplicationContext(),R.string.num_zero,Toast.LENGTH_SHORT).show();
                }else {
                    textViewNum.setText((Integer.parseInt(textViewNum.getText().toString())-1)+"");
                }
            }
        });
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayList.add(new ModelCart(id,name,description,image,textViewNum.getText().toString(),"offers"));
            }
        });
        fabCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(OfferActivity.this,CartActivity.class);
                intent.putExtra("arrayListCart",arrayList);
                intent.putExtra("additionals",HomeActivity.arrayListAdditionals);
                startActivity(intent);
            }
        });
    }

}
