package com.example.khalij.fastfood;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class FilterActivity extends AppCompatActivity {
    TextView textViewTitle,textViewLocate;
    Spinner spinnerCountry,spinnerCity,spinnerArea;
    Button buttonDone;
    ArrayList<String> arrayListCountries,arrayListCountriesID,arrayListCities,arrayListCitiesID,arrayListAreas,arrayListAreasID;
    Typeface type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        textViewTitle=(TextView)findViewById(R.id.textViewTitleFilter);
        textViewLocate=(TextView)findViewById(R.id.textViewLocate_filter);
        spinnerCountry=(Spinner)findViewById(R.id.spinnerCountry_filter);
        spinnerCity=(Spinner)findViewById(R.id.spinnerCity_filter);
        spinnerArea=(Spinner)findViewById(R.id.spinnerArea_filter);
        buttonDone=(Button)findViewById(R.id.buttonDone_filter);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textViewTitle.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_medium.ttf");
        textViewLocate.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        buttonDone.setTypeface(type);
        spinnerCity.setEnabled(false);
        spinnerArea.setEnabled(false);
        getCountries(Connection.BaseUrl+"country");
    }

    private void initEventDriven() {

    }

    public void getCountries(String url) {
        final ProgressDialog dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        arrayListCountries = new ArrayList<>();
        arrayListCountriesID = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    if (response != null) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JSONArray results = new JSONArray(response);
                    arrayListCountries.add(getResources().getString(R.string.country));
                    arrayListCountriesID.add("0");
                    for (int x = 0; x < results.length(); x++) {
                        arrayListCountries.add(results.getJSONObject(x).get("ar_name").toString());
                        arrayListCountriesID.add(results.getJSONObject(x).get("restId").toString());
                    }
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_view, arrayListCountries) {
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View v = super.getView(position, convertView, parent);
                            Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
                            ((TextView) v).setTypeface(externalFont);
                            if (((TextView) v).getText().toString().equals(getResources().getString(R.string.country))) {
                                ((TextView) v).setTextColor(Color.LTGRAY);
                            } else {
                                ((TextView) v).setTextColor(Color.BLACK);
                            }
                            return v;
                        }


                        @Override
                        public boolean isEnabled(int position) {
                            if (position == 0) {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            } else {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, View convertView,
                                                    ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if (position == 0) {
                                // Set the hint text color gray
                                tv.setTextColor(Color.LTGRAY);
                            } else {
                                tv.setTextColor(Color.BLACK);
                            }
                            Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
                            ((TextView) view).setTypeface(externalFont);
                            return view;
                        }
                    };
                    arrayAdapter.setDropDownViewResource(R.layout.spinner_view);
                    spinnerCountry.setAdapter(arrayAdapter);
                    spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            getCities(
                                    Connection.BaseUrl+"country/"
                                            + arrayListCountriesID.get(position).toString()
                            );
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error Message", error.getMessage());
                dialog.dismiss();
                initUI();
            }
        });
        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }
    public void getCities(String url) {
        spinnerCity.setEnabled(true);
        final ProgressDialog dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        arrayListCities = new ArrayList<>();
        arrayListCitiesID = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    if (response != null) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JSONArray results = new JSONArray(response);
                    arrayListCities.add(getResources().getString(R.string.town));
                    arrayListCitiesID.add("0");
                    for (int x = 0; x < results.length(); x++) {
                        arrayListCities.add(results.getJSONObject(x).get("ar_name").toString());
                        arrayListCitiesID.add(results.getJSONObject(x).get("restId").toString());
                    }
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_view, arrayListCities) {
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View v = super.getView(position, convertView, parent);
                            Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
                            ((TextView) v).setTypeface(externalFont);
                            if (((TextView) v).getText().toString().equals(getResources().getString(R.string.town))) {
                                ((TextView) v).setTextColor(Color.LTGRAY);
                            } else {
                                ((TextView) v).setTextColor(Color.BLACK);
                            }
                            return v;
                        }


                        @Override
                        public boolean isEnabled(int position) {
                            if (position == 0) {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            } else {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, View convertView,
                                                    ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if (position == 0) {
                                // Set the hint text color gray
                                tv.setTextColor(Color.LTGRAY);
                            } else {
                                tv.setTextColor(Color.BLACK);
                            }
                            Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
                            ((TextView) view).setTypeface(externalFont);
                            return view;
                        }
                    };
                    arrayAdapter.setDropDownViewResource(R.layout.spinner_view);
                    spinnerCity.setAdapter(arrayAdapter);
                    spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            getAreas(
                                    Connection.BaseUrl+"area/"
                                            + arrayListCitiesID.get(position).toString()
                            );
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error Message", error.getMessage());
                dialog.dismiss();
                initUI();
            }
        });
        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);

    }
    public void getAreas(String url) {
        spinnerArea.setEnabled(true);
        final ProgressDialog dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        arrayListAreas = new ArrayList<>();
        arrayListAreasID = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    if (response != null) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JSONArray results = new JSONArray(response);
                    arrayListAreas.add(getResources().getString(R.string.district));
                    arrayListAreasID.add("0");
                    for (int x = 0; x < results.length(); x++) {
                        arrayListAreas.add(results.getJSONObject(x).get("ar_name").toString());
                        arrayListAreasID.add(results.getJSONObject(x).get("restId").toString());
                    }
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_view, arrayListAreas) {
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View v = super.getView(position, convertView, parent);
                            Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
                            ((TextView) v).setTypeface(externalFont);
                            if (((TextView) v).getText().toString().equals(getResources().getString(R.string.district))) {
                                ((TextView) v).setTextColor(Color.LTGRAY);
                            } else {
                                ((TextView) v).setTextColor(Color.BLACK);
                            }
                            return v;
                        }


                        @Override
                        public boolean isEnabled(int position) {
                            if (position == 0) {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            } else {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, View convertView,
                                                    ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if (position == 0) {
                                // Set the hint text color gray
                                tv.setTextColor(Color.LTGRAY);
                            } else {
                                tv.setTextColor(Color.BLACK);
                            }
                            Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
                            ((TextView) view).setTypeface(externalFont);
                            return view;
                        }
                    };
                    arrayAdapter.setDropDownViewResource(R.layout.spinner_view);
                    spinnerArea.setAdapter(arrayAdapter);
                    buttonDone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error Message", error.getMessage());
                dialog.dismiss();
                initUI();
            }
        });
        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }

}
