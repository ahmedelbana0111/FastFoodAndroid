package com.example.khalij.fastfood;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MenuActivityFragment extends Fragment {
    View view;
    String id;
    ArrayList<String> arrayListMenuIds,arrayListNames;
    RecyclerView recyclerView;
    Spinner spinner;
    Boolean loading = false;
    RecyclerView.LayoutManager lm;
    String nextUrl = "null";
    String url;
    ArrayList<ModelFood> arrayListModel;
    RecyclerAdapterItem recyclerAdapter;
    public MenuActivityFragment() {
    }

    public static MenuActivityFragment newInstance(String id){
        MenuActivityFragment menuActivityFragment  = new MenuActivityFragment();
        Bundle bundle=new Bundle();
        bundle.putString("restId",id);
        menuActivityFragment.setArguments(bundle);
        return menuActivityFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_menu, container, false);
        id=getArguments().getString("restId");
        getVolley(Connection.BaseUrl+"branch/" +
                id +
                "/menu");
        initViews();
        initUI();
        initEventDriven();
        return view;
    }

    private void initViews() {
        spinner=(Spinner)view.findViewById(R.id.spinnerMenu);
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerMenu);
    }

    private void initUI() {
        lm = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(lm);
    }

    private void initEventDriven() {

    }

    public void getVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(getActivity()); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        arrayListMenuIds=new ArrayList<>();
        arrayListNames=new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    if (response != null) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JSONArray results = new JSONArray(response);

                    for (int x=0;x<results.length();x++){
                        arrayListMenuIds.add(results.getJSONObject(x).get("id").toString());
                        arrayListNames.add(results.getJSONObject(x).get("ar_name").toString());
                    }
                    Log.e("SIIIIZE",arrayListMenuIds.size()+"");
                    Log.e("siiiize",arrayListNames.size()+"");
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_view, arrayListNames) {
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View v = super.getView(position, convertView, parent);
                            Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_light.ttf");
                            ((TextView) v).setTypeface(externalFont);
                                ((TextView) v).setTextColor(Color.LTGRAY);
                                ((TextView) v).setTextColor(Color.BLACK);
                            return v;
                        }


                        @Override
                        public boolean isEnabled(int position) {

                                return true;

                        }

                        @Override
                        public View getDropDownView(int position, View convertView,
                                                    ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;

                                tv.setTextColor(Color.BLACK);

                            Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_light.ttf");
                            ((TextView) view).setTypeface(externalFont);
                            return view;
                        }
                    };
                    arrayAdapter.setDropDownViewResource(R.layout.spinner_view);
                    spinner.setAdapter(arrayAdapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            arrayListModel=new ArrayList<ModelFood>();

                            getVolley2(Connection.BaseUrl+"branch/menu/" +
                                    arrayListMenuIds.get(position).toString() +
                                    "/food");
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error Message", error.getMessage());
                dialog.dismiss();
                initUI();
            }
        });
        Volley.newRequestQueue(getContext()).add(stringRequest);
    }
    public void getVolley2(String url) {
        final ProgressDialog dialog = new ProgressDialog(getActivity()); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    if (response != null) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JSONObject results = new JSONObject(response);
                    JSONArray jsonArray = results.getJSONArray("data");
                    for (int x = 0; x < jsonArray.length(); x++) {
                        arrayListModel.add(new ModelFood(jsonArray.getJSONObject(x).get("id").toString()
                                , jsonArray.getJSONObject(x).get("ar_name").toString()
                                , jsonArray.getJSONObject(x).get("ar_additional").toString()
                                , jsonArray.getJSONObject(x).get("photo").toString()
                                , ""));
                    }
                    nextUrl = results.get("next_page_url").toString();
                    loading = !nextUrl.equals("null");
                    recyclerAdapter = new RecyclerAdapterItem(arrayListModel);
                    recyclerView.setAdapter(recyclerAdapter);
                    recyclerAdapter.notifyDataSetChanged();
                    recyclerView.addOnItemTouchListener(
                            new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) throws FileNotFoundException {
                                    Intent intent=new Intent(getActivity(),ItemDetailsActivity.class);
                                    intent.putExtra("restId",arrayListModel.get(position).getId().toString());
                                    startActivity(intent);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                }
                            })
                    );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error Message", error.getMessage());
                dialog.dismiss();
                initUI();
            }
        });
        Volley.newRequestQueue(getContext()).add(stringRequest);
    }
}
