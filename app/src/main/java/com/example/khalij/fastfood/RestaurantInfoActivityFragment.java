package com.example.khalij.fastfood;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A placeholder fragment containing a simple view.
 */
public class RestaurantInfoActivityFragment extends Fragment {
    View view;
    SliderLayout sliderShow;
    Button buttonReserve;
    ImageView imageViewPic;
    TextView textViewName,textViewAddress,textViewMinCharge,textViewDelivery,textViewAbout,textViewAboutRest;
    Typeface type;
    String id;

    public RestaurantInfoActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_restaurant_info, container, false);
        id=getArguments().getString("restId");
        initViews();
        initUI();
        initEventDriven();
        return view;
    }

    private void initViews() {
        buttonReserve=(Button)view.findViewById(R.id.buttonRerveRestInfo);
        imageViewPic=(ImageView)view.findViewById(R.id.imageViewPicRestInfo);
        textViewName=(TextView)view.findViewById(R.id.textViewNameRestInfo);
        textViewAddress=(TextView)view.findViewById(R.id.textViewAddressRestInfo);
        textViewMinCharge=(TextView)view.findViewById(R.id.textViewMinChargeRestInfo);
        textViewDelivery=(TextView)view.findViewById(R.id.textViewDeliveryRestInfo);
        textViewAbout=(TextView)view.findViewById(R.id.textViewAboutRestInfo);
        textViewAboutRest=(TextView)view.findViewById(R.id.textViewAboutRestRestInfo);
        sliderShow = (SliderLayout) view.findViewById(R.id.slider);
        sliderShow.setDuration(10000);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_regular.ttf");
        textViewName.setTypeface(type);
        textViewAddress.setTypeface(type);
        textViewMinCharge.setTypeface(type);
        textViewDelivery.setTypeface(type);
        textViewAboutRest.setTypeface(type);
        textViewAbout.setTypeface(type);
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_medium.ttf");
        buttonReserve.setTypeface(type);
        getVolley(Connection.BaseUrl+"branch/"+id);
    }

    private void initEventDriven() {
        buttonReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),ReserveTableActivity.class);
                intent.putExtra("restId",id);
                startActivity(intent);
            }
        });
    }

    public void getVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(getActivity()); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    if (response != null) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JSONObject results = new JSONObject(response);
                    textViewName.setText(results.getJSONObject("branch").get("ar_name").toString());
                    textViewAddress.setText(results.getJSONObject("branch").get("address").toString());
                    textViewMinCharge.append(results.getJSONObject("branch").get("min_charge").toString());
                    textViewDelivery.append(" : "+results.getJSONObject("branch").get("delivery").toString());
                    textViewAboutRest.setText(results.getJSONObject("branch").get("ar_details").toString());
                    Picasso.with(getContext())
                            .load(Connection.ImageUrl+
                                    results.getJSONObject("branch").get("logo").toString()).into(imageViewPic);
                    JSONArray jsonArray=results.getJSONArray("photos");
                    for (int x=0;x<jsonArray.length();x++){
                        sliderShow.addSlider(new TextSliderView(getActivity().getApplication()).description("").image(
                                Connection.ImageUrl+jsonArray.get(x).toString()
                        ));
                    }
                    sliderShow.setDuration(10000);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error Message", error.getMessage());
                dialog.dismiss();
                initUI();
            }
        });
        Volley.newRequestQueue(getContext()).add(stringRequest);
    }
    public static RestaurantInfoActivityFragment newInstance(String id){
        RestaurantInfoActivityFragment restaurantInfoActivityFragment  = new RestaurantInfoActivityFragment();
        Bundle bundle=new Bundle();
        bundle.putString("restId",id);
        restaurantInfoActivityFragment.setArguments(bundle);
        return restaurantInfoActivityFragment;
    }
}
