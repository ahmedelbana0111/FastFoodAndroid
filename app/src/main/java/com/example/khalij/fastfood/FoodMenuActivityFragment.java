package com.example.khalij.fastfood;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A placeholder fragment containing a simple view.
 */
public class FoodMenuActivityFragment extends Fragment {
    View view;
    public FoodMenuActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_food_menu, container, false);
        initViews();
        initUI();
        initEventDriven();
        return view;
    }

    private void initViews() {

    }

    private void initUI() {

    }

    private void initEventDriven() {

    }
}
