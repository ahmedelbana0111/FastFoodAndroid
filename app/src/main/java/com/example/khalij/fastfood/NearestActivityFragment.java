package com.example.khalij.fastfood;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A placeholder fragment containing a simple view.
 */
public class NearestActivityFragment extends Fragment {
    public static FragmentManager fragmentManager;
    View view;
    public NearestActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_nearest, container, false);
        fragmentManager = getActivity().getSupportFragmentManager();
        //?saddr=20.344,34.34&daddr=20.5666,45.345
        Intent intent = new Intent(getActivity(),MapActivity2.class);
        //        Uri.parse("http://maps.google.com/maps"));
        startActivity(intent);
        return view;
    }
}
