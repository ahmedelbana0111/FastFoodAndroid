package com.example.khalij.fastfood;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

/**
 * A placeholder fragment containing a simple view.
 */
public class SignUpRestaurantActivityFragment extends Fragment {
    View view;
    Typeface type;
    EditText editTextName,editTextPhone,editTextMail,editTextPassword,editTextConfirmPassword,editTextCommercial,editTextRestName;
    Spinner spinnerCountry,spinnerTown,spinnerDistrict,spinnerPeriod,spinnerCategory,spinnerRestCat;
    Button buttonSignUp;
    public SignUpRestaurantActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_sign_up_restaurant, container, false);
        initViews();
        initUI();
        initEventDriven();
        return view;
    }

    private void initViews() {
        editTextName=(EditText)view.findViewById(R.id.editTextName_signUpRest);
        editTextPhone=(EditText)view.findViewById(R.id.editTextPhone_signUpRest);
        editTextMail=(EditText)view.findViewById(R.id.editTextMail_signUpRest);
        editTextPassword=(EditText)view.findViewById(R.id.editTextPassWord_signUpRest);
        editTextConfirmPassword=(EditText)view.findViewById(R.id.editTextConfirmPassWord_signUpRest);
        editTextCommercial=(EditText)view.findViewById(R.id.editTextCommircial_signUpRest);
        editTextRestName=(EditText)view.findViewById(R.id.editTextRestName_signUpRest);
        spinnerCountry=(Spinner)view.findViewById(R.id.spinnerCountry_signUpRest);
        spinnerTown=(Spinner)view.findViewById(R.id.spinnerTown_signUpRest);
        spinnerDistrict=(Spinner)view.findViewById(R.id.spinnerDistrict_signUpRest);
        spinnerPeriod=(Spinner)view.findViewById(R.id.spinnerPeriod_signUpRest);
        spinnerCategory=(Spinner)view.findViewById(R.id.spinnerCategory_signUpRest);
        spinnerRestCat=(Spinner)view.findViewById(R.id.spinnerRestaurantCat_signUpRest);
        buttonSignUp=(Button)view.findViewById(R.id.buttonSignUp_signUpRest);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_regular.ttf");
        buttonSignUp.setTypeface(type);
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_light.ttf");
        editTextPhone.setTypeface(type);
        editTextName.setTypeface(type);
        editTextConfirmPassword.setTypeface(type);
        editTextPassword.setTypeface(type);
        editTextMail.setTypeface(type);
        editTextCommercial.setTypeface(type);
        editTextRestName.setTypeface(type);
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_medium.ttf");
    }

    private void initEventDriven() {

    }
}
