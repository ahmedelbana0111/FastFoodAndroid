package com.example.khalij.fastfood;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainHomeActivityFragment extends Fragment {
    View view;
    Button buttonFamilies, buttonHighRests, buttonAllRests, buttonMostOrder, buttonRestAdvs, buttonRestOffers;
    Typeface type;

    public MainHomeActivityFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main_home, container, false);
        initViews();
        initUI();
        initEventDriven();
        return view;
    }

    private void initViews() {
        buttonFamilies = (Button) view.findViewById(R.id.buttonFamilies);
        buttonHighRests = (Button) view.findViewById(R.id.buttonHighRests);
        buttonAllRests = (Button) view.findViewById(R.id.buttonAllRests);
        buttonMostOrder = (Button) view.findViewById(R.id.buttonMostOrder);
        buttonRestAdvs = (Button) view.findViewById(R.id.buttonRestAdvs);
        buttonRestOffers = (Button) view.findViewById(R.id.buttonRestOffers);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_regular.ttf");
        buttonFamilies.setTypeface(type);
        buttonHighRests.setTypeface(type);
        buttonAllRests.setTypeface(type);
        buttonMostOrder.setTypeface(type);
        buttonRestAdvs.setTypeface(type);
        buttonRestOffers.setTypeface(type);
    }

    private void initEventDriven() {
        buttonFamilies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SecondHomeActivity.class);
                intent.putExtra("url", Connection.BaseUrl+"branch/filter/type/productive_family");
                intent.putExtra("type", "other");
                intent.putExtra("title", getResources().getString(R.string.family));
                startActivity(intent);
            }
        });
        buttonHighRests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SecondHomeActivity.class);
                intent.putExtra("url", Connection.BaseUrl+"branch/filter/type/smart_restaurant");
                intent.putExtra("type", "other");
                intent.putExtra("title", getResources().getString(R.string.high_rest));
                startActivity(intent);
            }
        });
        buttonAllRests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SecondHomeActivity.class);
                intent.putExtra("url", Connection.BaseUrl+"branch/filter/type/restaurant");
                intent.putExtra("type", "other");
                intent.putExtra("title", getResources().getString(R.string.all_rest));
                startActivity(intent);
            }
        });
        buttonMostOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SecondHomeActivity.class);
                intent.putExtra("url", Connection.BaseUrl+"most/order");
                intent.putExtra("type", "other");
                intent.putExtra("title", getResources().getString(R.string.most_order));
                startActivity(intent);
            }
        });
        buttonRestAdvs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SecondHomeActivity.class);
                intent.putExtra("url", Connection.BaseUrl+"ad");
                intent.putExtra("type", "adv");
                intent.putExtra("title", getResources().getString(R.string.rest_adv));
                startActivity(intent);
            }
        });
        buttonRestOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OffersActivity.class);
                intent.putExtra("url", Connection.BaseUrl+"offer");
                intent.putExtra("title", getResources().getString(R.string.rest_offers));
                startActivity(intent);
            }
        });
    }
}
