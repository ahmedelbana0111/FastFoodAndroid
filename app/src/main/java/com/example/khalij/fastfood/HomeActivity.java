package com.example.khalij.fastfood;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String botNav = "",name;
    FrameLayout frameLayout;
    Menu m;
    DrawerLayout drawer;
    TextView textView;
    Typeface type;
    Fragment fragment;
    public static String token;
    public static ArrayList<ModelCart> arrayListCart;
    public static ArrayList<String> arrayListAdditionals;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (!botNav.equals("1")) {
                        textView.setText(R.string.home);
                        FragmentManager fragmentManager2 = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
                        MainHomeActivityFragment fragment2 = new MainHomeActivityFragment();
                        fragmentTransaction2.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit);
                        fragmentTransaction2.replace(R.id.content, fragment2);
                        fragmentTransaction2.commit();
                        botNav = "1";
                    }
                    return true;
                case R.id.navigation_fav:
                    if (!botNav.equals("2")) {
                        textView.setText(R.string.my_orders);
                        FragmentManager fragmentManager3 = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction3 = fragmentManager3.beginTransaction();
                        FavouriteActivityFragment fragment3 = new FavouriteActivityFragment();
                        fragmentTransaction3.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit);
                        fragmentTransaction3.replace(R.id.content, fragment3);
                        fragmentTransaction3.commit();
                        botNav = "2";
                    }
                    return true;
                case R.id.navigation_nearest:
                    if (!botNav.equals("3")) {
                        textView.setText(R.string.nearest_rest);
                        FragmentManager fragmentManager4 = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction4 = fragmentManager4.beginTransaction();
                        MapActivity2Fragment fragment4 = new MapActivity2Fragment().newInstance("0","nearest");
                        fragmentTransaction4.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit);
                        fragmentTransaction4.replace(R.id.content, fragment4);
                        fragmentTransaction4.commit();
                        botNav = "3";
                    }
                    return true;
            }
            return true;
        }

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initViews();
        Log.e("mobile_token",getSharedPreferences("testFCM", MODE_PRIVATE).getString("mobile_token",""));
        arrayListCart=new ArrayList<>();
        arrayListAdditionals=new ArrayList<>();
        token=getIntent().getStringExtra("token");
        name=getIntent().getStringExtra("name");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_home);
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//
//        fragment = new Fragment();
//        fragmentTransaction.add(R.restId.map, fragment);
//        fragmentTransaction.commit();
        frameLayout = (FrameLayout) findViewById(R.id.content);
        m = navigation.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                    drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });
        navigationView.setItemIconTintList(null);
        m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

        initUI();
//        View hView = navigationView.getHeaderView(0);
//        TextView nav_user = (TextView) hView.findViewById(R.restId.textViewName_headerHome);
//        nav_user.setTypeface(type);
//        nav_user.setText(name);
        initEventDriven();
    }

    private void initEventDriven() {

    }

    private void initUI() {
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textView.setTypeface(type);
    }

    private void initViews() {
        textView=(TextView)findViewById(R.id.homeTitle);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Intent intent=new Intent(HomeActivity.this,MyProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_locate) {
            Intent intent=new Intent(HomeActivity.this,FilterActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_log_out) {
            Intent intent=new Intent(HomeActivity.this,SignInActivity.class);
            startActivity(intent);
            finish();
        }else if (id == R.id.nav_log_in) {
            Intent intent=new Intent(HomeActivity.this,SignInActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }
    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
}
