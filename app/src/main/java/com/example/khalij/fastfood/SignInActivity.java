package com.example.khalij.fastfood;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignInActivity extends AppCompatActivity {
    public String token;
    EditText editTextMail, editTextPassWord;
    Button buttonSignIn, buttonSignUp;
    TextView textViewForgetPassWord1, textViewForgetPassWord2;
    Typeface type;
    JSONObject obj;
    private String TAG = HomeActivity.class.getSimpleName();

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        }
        initViews();
        initUI();
//        Log.e("TOOOKEN",token);
        initEventDriven();
    }

    private void initViews() {
        editTextMail = (EditText) findViewById(R.id.editTextPhone_signIn);
        editTextPassWord = (EditText) findViewById(R.id.editTextPassWord_signIn);
        buttonSignIn = (Button) findViewById(R.id.buttonSignIn_signIn);
        buttonSignUp = (Button) findViewById(R.id.buttonNewAccount_signIn);
        textViewForgetPassWord1 = (TextView) findViewById(R.id.textViewForgetPassWord1_signIn);
        textViewForgetPassWord2 = (TextView) findViewById(R.id.textViewForgetPassWord2_signIn);
    }

    private void initUI() {
        Log.e("mobile_token", getSharedPreferences("testFCM", MODE_PRIVATE).getString("mobile_token", ""));
        textViewForgetPassWord2.setPaintFlags(textViewForgetPassWord2.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        buttonSignIn.setTypeface(type);
        buttonSignUp.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
        editTextPassWord.setTypeface(type);
        editTextMail.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_medium.ttf");
        textViewForgetPassWord1.setTypeface(type);
        textViewForgetPassWord2.setTypeface(type);
    }

    private void initEventDriven() {
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextPassWord.getText().toString().equals("") || editTextMail.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.fill_password), Toast.LENGTH_LONG).show();
                    return;
                }
                postVolley(Connection.BaseUrl + "login");
            }
        });
        textViewForgetPassWord2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void postVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response is: ", response.toString());
                        try {
                            obj = new JSONObject(response);
//                            Log.e("OBJ", obj.get("restId").toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (obj.get("status").toString().equals("invalid credentails")) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.wrong_password), Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please), Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            try {
                                Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                                intent.putExtra("token", obj.get("token").toString());
                                intent.putExtra("name", obj.getJSONObject("user").get("name").toString());
                                SharedPreferences sharedPreferences = getSharedPreferences("fastFood", MODE_PRIVATE);
                                SharedPreferences.Editor edit = sharedPreferences.edit();
                                edit.putBoolean("isLogin", true);
                                edit.putString("token", obj.get("token").toString());
                                edit.putString("name", obj.getJSONObject("user").get("name").toString());
                                edit.apply();
                                startActivity(intent);
                                dialog.dismiss();
                                finish();
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                }
                if (error instanceof TimeoutError) {
                    Log.e("Volley", "TimeoutError");
                } else if (error instanceof NoConnectionError) {
                    Log.e("Volley", "NoConnectionError");
                } else if (error instanceof AuthFailureError) {
                    Log.e("Volley", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.e("Volley", "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.e("Volley", "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.e("Volley", "ParseError");
                }
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                Log.e("Response is: ", "That didn't work!");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("password", editTextPassWord.getText().toString());
                params.put("phone", editTextMail.getText().toString());
                params.put("mobile_type", "android");
                params.put("mobile_token", getSharedPreferences("testFCM", MODE_PRIVATE).getString("mobile_token", ""));
                Log.e("goooooo", params.toString());
                return params;
            }

        };
// Add the request to the RequestQueue.
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

}
