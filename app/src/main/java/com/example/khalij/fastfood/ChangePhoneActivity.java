package com.example.khalij.fastfood;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ChangePhoneActivity extends AppCompatActivity {
    TextView textViewTitle,textViewEnter;
    EditText editTextPhoneNum;
    Button buttonChange;
    Typeface type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_phone);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        textViewTitle=(TextView)findViewById(R.id.textViewTitle_changePhone);
        textViewEnter=(TextView)findViewById(R.id.textViewEnter_changePhone);
        editTextPhoneNum=(EditText)findViewById(R.id.editTextPhoneNum_changePhone);
        buttonChange=(Button)findViewById(R.id.buttonChange_changePhone);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textViewTitle.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_medium.ttf");
        buttonChange.setTypeface(type);
        textViewEnter.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        editTextPhoneNum.setTypeface(type);
    }

    private void initEventDriven() {
        buttonChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

}
