package com.example.khalij.fastfood;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ReserveTableActivity extends AppCompatActivity {
    static final int DATE_DIALOG_ID = 999;
    TextView textViewTitle,textViewReserve;
    EditText editTextPersonsNum,editTextDate,editTextTime,editTextOtherPhones,editTextOccasionTable;
    Button buttonAdd;
    String time;
    Typeface type;
    String id;
    private int year;
    private int month;
    private int day;
    JSONObject obj;
    String restId;
    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // set selected date into textview
            editTextDate.setText(new StringBuilder().append(year).append("-").append(month + 1)
                    .append("-").append(day)
            );


        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve_table);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        id=getIntent().getStringExtra("restId");
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        textViewTitle=(TextView)findViewById(R.id.textViewTableTitle);
        textViewReserve=(TextView)findViewById(R.id.textViewReserveTable);
        editTextPersonsNum=(EditText)findViewById(R.id.editTextPersonsNumTable);
        editTextDate=(EditText)findViewById(R.id.editTextDateTable);
        editTextTime=(EditText)findViewById(R.id.editTextTimeTable);
        editTextOtherPhones=(EditText)findViewById(R.id.editTextOtherPhonesTable);
        editTextOccasionTable=(EditText)findViewById(R.id.editTextOccasionTable);
        buttonAdd=(Button)findViewById(R.id.buttonAddTable);
        textViewTitle=(TextView)findViewById(R.id.textViewTableTitle);
        Log.e("TOKEEEEEEN",HomeActivity.token);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textViewTitle.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_medium.ttf");
        textViewReserve.setTypeface(type);
        buttonAdd.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        editTextPersonsNum.setTypeface(type);
        editTextDate.setTypeface(type);
        editTextTime.setTypeface(type);
        editTextOtherPhones.setTypeface(type);
        editTextOccasionTable.setTypeface(type);
        editTextDate.setKeyListener(null);
        editTextTime.setKeyListener(null);
    }

    private void initEventDriven() {
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postVolley(Connection.BaseUrl+"branch/" +
                        restId+
                        "/reservation");
            }
        });
        editTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("DATE","EXIST");
                setCurrentDateOnView();
                showDialog(DATE_DIALOG_ID);
            }
        });
        editTextTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("TIME","EXIST");
                showDialog(0);
            }
        });
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        setTheme(R.style.MyTimePickerTheme);
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        year, month, day);
            case 0:

                TimePickerDialog timeDlg = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                // TODO Auto-generated method stub

                                if (hourOfDay > 12) {
                                    hourOfDay = hourOfDay - 12;
                                    time = " PM";
                                } else if (hourOfDay < 12 && hourOfDay != 0) {
                                    time = " AM";
                                } else if (hourOfDay == 12) {
                                    time = " PM";
                                } else if (hourOfDay == 0) {
                                    hourOfDay = 12;
                                    time = " AM";
                                }

//                                Toast.makeText(
//                                        getApplicationContext(),
//                                        new StringBuilder().append((hourOfDay))
//                                                .append(":").append((minute))
//                                                .append(time), Toast.LENGTH_SHORT)
//                                        .show();
                                time = hourOfDay + ":" + minute + time;
                                editTextTime.setText(time);
                            }
                        }, 12, 00, false);


                timeDlg.setOnCancelListener(new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        // TODO Auto-generated method stub
//                        Toast.makeText(getApplicationContext(), "Dismiss",
//                                Toast.LENGTH_SHORT).show();
                    }
                });
                return timeDlg;
        }
        return null;
    }
    public void setCurrentDateOnView() {


        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


    }



    public void postVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response is: ", response.toString());
                        try {
                            obj = new JSONObject(response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                }
                if (error instanceof TimeoutError) {
                    Log.e("Volley", "TimeoutError");
                } else if (error instanceof NoConnectionError) {
                    Log.e("Volley", "NoConnectionError");
                } else if (error instanceof AuthFailureError) {
                    Log.e("Volley", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.e("Volley", "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.e("Volley", "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.e("Volley", "ParseError");
                }
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                Log.e("Response is: ", "That didn't work!");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
//                params.put("number",)
//                params.put("occasion",);
//                params.put("date",);
//                params.put("time",);
//                params.put("other_phone",);
//                params.put("token",HomeActivity.token);

                return params;
            }

        };
// Add the request to the RequestQueue.
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
}
