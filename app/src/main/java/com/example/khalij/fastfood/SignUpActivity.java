package com.example.khalij.fastfood;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {
    Typeface type;
    EditText editTextName,editTextPhone,editTextMail,editTextPassword,editTextConfirmPassword;
    Button buttonSignUp;
    JSONObject obj;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        editTextName=(EditText)findViewById(R.id.editTextName_signUpUser);
        editTextPhone=(EditText)findViewById(R.id.editTextPhone_signUpUser);
        editTextMail=(EditText)findViewById(R.id.editTextMail_signUpUser);
        editTextPassword=(EditText)findViewById(R.id.editTextPassWord_signUpUser);
        editTextConfirmPassword=(EditText)findViewById(R.id.editTextConfirmPassWord_signUpUser);
        buttonSignUp=(Button)findViewById(R.id.buttonSignUp_signUpUser);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_regular.ttf");
        buttonSignUp.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_light.ttf");
        editTextPhone.setTypeface(type);
        editTextName.setTypeface(type);
        editTextConfirmPassword.setTypeface(type);
        editTextPassword.setTypeface(type);
        editTextMail.setTypeface(type);
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_medium.ttf");
    }

    private void initEventDriven() {
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextMail.getText().equals("")
                        ||editTextMail.getText().toString().contains(" ")
                        ||!editTextMail.getText().toString().contains("@")
                        ||!editTextMail.getText().toString().contains(".")){
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.fill_mail),Toast.LENGTH_LONG).show();
                    return;
                }
                if (editTextPassword.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.fill_password),Toast.LENGTH_LONG).show();
                    return;
                }
                if(editTextConfirmPassword.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.fill_confirm),Toast.LENGTH_LONG).show();
                    return;
                }
                if(editTextName.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.fill_name),Toast.LENGTH_LONG).show();
                    return;
                }
                if(editTextPhone.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.fill_phone),Toast.LENGTH_LONG).show();
                    return;
                }
                postVolley(Connection.BaseUrl+"register");
            }
        });
    }

    public void postVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response is: ", response.toString());
                        try {
                            obj = new JSONObject(response);
                            Log.e("OBJ", obj.get("restId").toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (obj.get("status").toString().equals("0")) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.wrong_password), Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please), Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            finish();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                }
                if (error instanceof TimeoutError) {
                    Log.e("Volley", "TimeoutError");
                } else if (error instanceof NoConnectionError) {
                    Log.e("Volley", "NoConnectionError");
                } else if (error instanceof AuthFailureError) {
                    Log.e("Volley", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.e("Volley", "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.e("Volley", "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.e("Volley", "ParseError");
                }
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                Log.e("Response is: ", "That didn't work!");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("password", editTextPassword.getText().toString());
                params.put("email", editTextMail.getText().toString());
                params.put("name", editTextName.getText().toString());
                params.put("phone", editTextPhone.getText().toString());
                params.put("role", "user");
                params.put("mobile_type","android");
                params.put("mobile_token",getSharedPreferences("testFCM",MODE_PRIVATE).getString("mobile_token",""));
                Log.e("goooooo", params.toString());
                return params;
            }

        };
// Add the request to the RequestQueue.
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

}
