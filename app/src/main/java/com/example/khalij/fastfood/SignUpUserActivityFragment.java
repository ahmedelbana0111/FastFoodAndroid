package com.example.khalij.fastfood;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * A placeholder fragment containing a simple view.
 */
public class SignUpUserActivityFragment extends Fragment {
    View view;
    Typeface type;
    EditText editTextName,editTextPhone,editTextMail,editTextPassword,editTextConfirmPassword;
    Button buttonSignUp;
    public SignUpUserActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_sign_up_user, container, false);
        initViews();
        initUI();
        initEventDriven();
        return view;
    }

    private void initViews() {
        editTextName=(EditText)view.findViewById(R.id.editTextName_signUpUser);
        editTextPhone=(EditText)view.findViewById(R.id.editTextPhone_signUpUser);
        editTextMail=(EditText)view.findViewById(R.id.editTextMail_signUpUser);
        editTextPassword=(EditText)view.findViewById(R.id.editTextPassWord_signUpUser);
        editTextConfirmPassword=(EditText)view.findViewById(R.id.editTextConfirmPassWord_signUpUser);
        buttonSignUp=(Button)view.findViewById(R.id.buttonSignUp_signUpUser);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_regular.ttf");
        buttonSignUp.setTypeface(type);
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_light.ttf");
        editTextPhone.setTypeface(type);
        editTextName.setTypeface(type);
        editTextConfirmPassword.setTypeface(type);
        editTextPassword.setTypeface(type);
        editTextMail.setTypeface(type);
        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/din_next_medium.ttf");
    }

    private void initEventDriven() {

    }
}
