package com.example.khalij.fastfood;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class OffersActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerAdapterOffer recyclerAdapter;
    ArrayList<ModelOffer> arrayListModel;
    RecyclerView.LayoutManager lm;
    Boolean loading = false;
    String nextUrl = "null";
    String url;
    TextView textViewTitle;
    Typeface type;
    String title;
    FloatingActionButton floatFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        url = getIntent().getStringExtra("url");
        title=getIntent().getStringExtra("title");
        initViews();
        initUI();
        initEventDriven();
    }

    private void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewOffers);
        textViewTitle=(TextView)findViewById(R.id.textViewTitle_offers);
        floatFilter=(FloatingActionButton)findViewById(R.id.fab_filter1);
    }

    private void initUI() {
        type = Typeface.createFromAsset(getAssets(), "fonts/din_next_bold.ttf");
        textViewTitle.setText(title);
        textViewTitle.setTypeface(type);
        lm = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(lm);
        arrayListModel = new ArrayList<>();
        getVolley(url);
    }

    private void initEventDriven() {
        floatFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(OffersActivity.this,FilterActivity.class);
                startActivity(intent);
            }
        });
    }

    public void getVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    if (response != null) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JSONObject results = new JSONObject(response);
                    JSONArray jsonArray = results.getJSONArray("data");
                    for (int x = 0; x < jsonArray.length(); x++) {
                        arrayListModel.add(new ModelOffer(jsonArray.getJSONObject(x).get("branch_id").toString()
                                , jsonArray.getJSONObject(x).get("ar_name").toString()
                                , jsonArray.getJSONObject(x).get("id").toString()
                                , jsonArray.getJSONObject(x).get("ar_describe").toString()
                                , jsonArray.getJSONObject(x).get("photo").toString()
                                , jsonArray.getJSONObject(x).get("price").toString()));
                    }
                    nextUrl = results.get("next_page_url").toString();
                    loading = !nextUrl.equals("null");
                    recyclerAdapter = new RecyclerAdapterOffer(arrayListModel);
                    recyclerView.setAdapter(recyclerAdapter);
                    recyclerAdapter.notifyDataSetChanged();
                    recyclerView.addOnItemTouchListener(
                            new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) throws FileNotFoundException {
                                    Intent intent=new Intent(OffersActivity.this,OfferActivity.class);
                                    intent.putExtra("restId",arrayListModel.get(position).getId().toString());
                                    intent.putExtra("id",arrayListModel.get(position).getOfferId().toString());
                                    intent.putExtra("name",arrayListModel.get(position).getName().toString());
                                    intent.putExtra("price",arrayListModel.get(position).getPrice().toString());
                                    intent.putExtra("description",arrayListModel.get(position).getInfo().toString());
                                    intent.putExtra("image",arrayListModel.get(position).getImageURL());
                                    startActivity(intent);
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {
                                }
                            })
                    );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error Message", error.getMessage());
                dialog.dismiss();
                initUI();
            }
        });
        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }

}
