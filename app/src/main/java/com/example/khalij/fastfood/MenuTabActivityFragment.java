package com.example.khalij.fastfood;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MenuTabActivityFragment extends Fragment {
    View view;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    ArrayList<ModelRestaurant> arrayListModel;
    RecyclerView.LayoutManager lm;
    Boolean loading = false;
    String nextUrl = "null";
    String id;
    String url;
    public MenuTabActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_menu_tab, container, false);
        initViews();
        id=getArguments().getString("restId");
        initUI();
        initEventDriven();
        return view;
    }

    private void initViews() {
        recyclerView=(RecyclerView) view.findViewById(R.id.recyclerViewMenu);
    }

    private void initUI() {
        lm = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(lm);
        arrayListModel = new ArrayList<>();
        getVolley(url);
    }

    private void initEventDriven() {

    }

    public void getVolley(String url) {
        final ProgressDialog dialog = new ProgressDialog(getActivity()); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("Response", response);
                    if (response != null) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getContext(), "حدث خطأ ما..!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JSONObject results = new JSONObject(response);
                    JSONArray jsonArray=results.getJSONArray("data");
                    for(int x=0;x<jsonArray.length();x++){

                    }
                    nextUrl = results.get("next_page_url").toString();
                    loading = !nextUrl.equals("null");
                    recyclerAdapter = new RecyclerAdapter(arrayListModel);
                    recyclerView.setAdapter(recyclerAdapter);
                    recyclerAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error Message", error.getMessage());
                dialog.dismiss();
                initUI();
            }
        });
        Volley.newRequestQueue(getContext()).add(stringRequest);
    }
    public static MenuTabActivityFragment newInstance(String id){
        MenuTabActivityFragment menuTabActivityFragment  = new MenuTabActivityFragment();
        Bundle bundle=new Bundle();
        bundle.putString("restId",id);
        menuTabActivityFragment.setArguments(bundle);
        return menuTabActivityFragment;
    }
}
