package com.example.khalij.fastfood;

/**
 * Created by elban on 2016-08-14.
 */
public class ModelFood {
    private String id;
    private String name;
    private String info;
    private String imageURL;
    private String price;
    public ModelFood() {
    }

    public ModelFood(String id, String name, String info, String imageURL,String price) {
        this.setId(id);
        this.setName(name);
        this.setInfo(info);
        this.setImageURL(imageURL);
        this.setPrice(price);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

}
