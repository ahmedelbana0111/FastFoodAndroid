package com.example.khalij.fastfood;

/**
 * Created by elban on 2016-08-14.
 */
public class ModelCart {
    private String id;
    private String name;
    private String info;
    private String imageURL;
    private String count;
    private String modelName;

    public ModelCart() {
    }

    public ModelCart(String id, String name, String info, String imageURL, String count,String modelName) {
        this.setId(id);
        this.setName(name);
        this.setInfo(info);
        this.setImageURL(imageURL);
        this.setCount(count);
        this.setModelName(modelName);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }


    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}
